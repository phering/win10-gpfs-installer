﻿function Get-ScriptDirectory
{
    $Invocation = (Get-Variable MyInvocation -Scope 1).Value
    Split-Path $Invocation.MyCommand.Path
}
$ExecutionPath = Get-ScriptDirectory

$cygwinInstall = $ExecutionPath + "\packages\setup-x86_64.exe"
$cyg1 = "--local-install"
$cyg2 = "--no-shortcuts"
$cyg3 = "--no-desktop"
$cyg4 = "--quiet-mode"
$cyg5 = "--root"
$cyg6 = "C:\cygwin64"
$cyg7 = "--arch"
$cyg8 = "x86_64"
$cyg9 = "--local-package-dir"
$cyg10 = $ExecutionPath + "\packages\cygwinpackages\"
$cyg11 = "--packages"
$cyg12 = "diffutils,flip,m4,mksh,perl,procps-ng,openssh,inetutils"

& $cygwinInstall $cyg1 $cyg2 $cyg3 $cyg4 $cyg5 $cyg6 $cyg7 $cyg8 $cyg9 $cyg10 $cyg11 $cyg12
