﻿if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

 $domainRootUsername = [System.Environment]::GetEnvironmentVariable('DOMAINROOTUSER','machine')
 $RootPassword = [System.Environment]::GetEnvironmentVariable('ROOTPASSWORD','machine')

 $arg1 = "C:\WINDOWS\gpfs_cygwin\usr\lpp\mmfs\win\mmwinservctl.bat"
 $arg2 = "set"
 $arg3 = "--account"
 $arg4 = "$domainRootUsername"
 $arg5 = "--password"
 $arg6 = "$RootPassword"
 $arg7 = "--remote-shell"
 $arg8 = "no"
 $arg9 = "-N"
 $arg10 = "$env:COMPUTERNAME"

 & $arg1 $arg2 $arg3 $arg4 $arg5 $arg6 $arg7 $arg8 $arg9 $arg10
