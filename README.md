# README #

This is the start of an automated Windows 10 installer for Native PIXSTOR client

HOW TO USE:
Double click batch file "Start-script.bat" as an Administrator user. Click allow on all popups.
If the GPFS terminal applications do not appear on your desktop after the installation is complete,
something went wrong and you'll need to troubleshoot where the error occured.

### What is this repository for? ###

To make the install process on Windows much easier

### Who do I talk to? ###

Peter Hering

###Outstanding tasks###

- Clean up Installer
	* Use Installer for all tasks currently being done by PS script.
	* Figure out uninstaller for all needed packages
	* Add more logging 
	* Silence all the prompts for install 
	
- Add full domain integration
	* figure out adding a root AD user if possible on credential prompt



### Completed ###
- Add full domain integration
	* allow for non "root" user with multicluster selection
	* sanatize scripts to be ok with whatever is entered at the beginning as user and domain
- package into complete Windows installation