###############################################################################
###############################################################################
## Windows 10 script to set prereqs and install all GPFS components.         ##
## GPFS version 5.0.2.3                                                      ##
##                                                                           ##
##                                                                           ##
##         Peter Hering                                                      ##
###############################################################################
###############################################################################
# Elivate shell to run as Administrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }


function Get-ScriptDirectory
{
    $Invocation = (Get-Variable MyInvocation -Scope 1).Value
    Split-Path $Invocation.MyCommand.Path
}

$ExecutionPath = Get-ScriptDirectory

#
#$Domain = Read-Host -Prompt 'Input AD Username, if not AD bound, leave blank'
#$RootUsername = Read-Host -Prompt 'Input root username'
#$RootPassword = Read-Host -Prompt 'Input root password'
#
#

# set permissions on install folder

$Acl = Get-Acl $ExecutionPath
$Ar = New-Object System.Security.AccessControl.FileSystemAccessRule("everyone", "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
$Acl.SetAccessRule($Ar)
Set-Acl $ExecutionPath $Acl


$Domain = $env:USERDOMAIN
$RootUsername = "root"
$RootPassword = "Edison2!"
$Password = ConvertTo-SecureString $RootPassword -AsPlainText -Force
$joinDomainPlusUser = $Domain + "\" + $RootUsername
$domainRootUsername = "$joinDomainPlusUser"
[System.Environment]::SetEnvironmentVariable('ROOTUSERNAME', $RootUsername,[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('ROOTPASSWORD', $RootPassword,[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('DOMAINROOTUSER', $joinDomainPlusUser,[System.EnvironmentVariableTarget]::Machine)

# Setup "root" user for Pixstor Administration
echo "Verifying Root user"

 $userExists = Get-LocalUser | Where-Object {$_.Name -eq "$RootUsername"}
 $userIsLocalAdmin = Get-LocalGroupMember -Group "Administrators"  | Where-Object {$_.Name -eq "$domainRootUsername"}

 if ($userExists.Name -eq $RootUsername) {
    echo "User Exists"
    if ($userIsLocalAdmin.name -eq $domainRootUsername){
        echo "User is Local Admin"
    } else {
        echo "Adding User to local Administrator Group"
        Add-LocalGroupMember -Group "Administrators" -Member "$RootUsername"
    }

 } else {
    echo "Creating Root user"
    New-LocalUser "$RootUsername" -Password $Password -FullName "Pixstor Root Account" -Description "Root account for Native client access to Pixstor"
    Add-LocalGroupMember -Group "Administrators" -Member "$RootUsername"
 }


# Check for Firewalls

echo "Checking for active firewalls"
    # Check Public firewall Profile and disable if active
    $sysFirewallreg1 = Get-ItemProperty -path "HKLM:\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile" -Name EnableFirewall | Select-Object -ExpandProperty EnableFirewall
        If ($sysFirewallreg1 -eq 1) {
            Set-NetFirewallProfile -Profile Public -Enabled False
        }

    # Check Private firewall Profile and disable if active
    $sysFirewallreg2 = Get-ItemProperty -path "HKLM:\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile" -Name EnableFirewall | Select-Object -ExpandProperty EnableFirewall
        If ($sysFirewallreg2 -eq 1) {
            Set-NetFirewallProfile -Profile Private -Enabled False
        }

    # Check Domain firewall Profile and disable if active
    $sysFirewallreg3 = Get-ItemProperty -path "HKLM:\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile" -Name EnableFirewall | Select-Object -ExpandProperty EnableFirewall
        If ($sysFirewallreg3 -eq 1) {
            Set-NetFirewallProfile -Profile Domain -Enabled False
        }


# Stop Realtime Virus Scan For good

$checkMpPreferences = Get-MpPreference
    if ($checkMpPreferences.disableRealtimeMonitoring -eq "False") {
        echo "Stopping real time virus protection"
        Set-MpPreference -DisableRealtimeMonitoring $true
    }

    $searchText = "DisableAntiSpyware"
    $path = "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender"
    $entryCheck = ""
    $RegKey = (Get-ItemProperty $path)
    $RegKey.PSObject.Properties | ForEach-Object {
      If($_.Name -like $searchText){
          $entryCheck = 1
          $realtimeVirusCheck = Get-ItemProperty -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name DisableAntiSpyware | Select-Object -ExpandProperty DisableAntiSpyware
          If (-NOT ($realtimeVirusCheck -eq 1)) {
              Set-Itemproperty -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -value "1"
          }
      }
    }
      If($entryCheck -ne 1) {
        New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Value "1" -PropertyType "DWORD"
      }

# Check For Sophos ###NOTE- Get Service names needed for this check####

$SophosService = "Sophos"
If (Get-Service $SophosService -ErrorAction SilentlyContinue) {
    If((Get-Service $SophosService).Status -eq 'Running') {
        Stop-Service $SophosService
        Write-Host "Stopping $SophosService"
    } Else {
        Write-Host "$SophosService found, but it is not running."
    }
} Else {
    Write-Host "$SophosService not found"
}

# Check registry entries that are important
$obcaseinsensitive = Get-ItemProperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Kernel" -Name obcaseinsensitive | Select-Object -ExpandProperty obcaseinsensitive
        If ($obcaseinsensitive -eq 0) {
            Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Kernel" -Name obcaseinsensitive -Value "1"
        }


# Add root user to login as a service

$loginAsServicePath = $ExecutionPath + "\lib\logonasservice.ps1"
& $loginAsServicePath "$domainRootUsername"


# Verify UAC is on
$UACStatus = (Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System).ConsentPromptBehaviorAdmin
    If ($UACStatus -eq 0) {
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name ConsentPromptBehaviorAdmin -Value "5"
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name PromptOnSecureDesktop -Value "1"
    }

# Set powermode to Performance/Ultimate

powercfg /setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

# Call script to install Cygwin
echo "Installing Cygwin"

$cygpath = $ExecutionPath + "\lib\cygwin-install.ps1"

$Credential = New-Object System.Management.Automation.PSCredential $domainRootUsername, $Password
Start-Process $PSHOME\powershell.exe -Credential $Credential -ArgumentList ("-file $cygpath") -WorkingDirectory 'C:\Windows\System32' -Wait

# Setup SSH with Cygwin
echo "Configuring Cygwin"
$cygwinSetupSsh = $ExecutionPath + "\lib\setupssh.ps1"
Start-Process $PSHOME\powershell.exe -Credential $Credential -ArgumentList ("-file $cygwinSetupSsh") -WorkingDirectory 'C:\Windows\System32' -Wait


# Add root to log on for serivce cygsshd  ###Note - need to get recovery actions added as well
$service = gwmi win32_service -computer $env:COMPUTERNAME -filter "name='cygsshd'"
$service.change($null,$null,$null,$null,$null,$null,"$domainRootUsername","$RootPassword")
  {write-host "$ServerN -> Service Started Successfully"}


# Start "cygsshd"
net start cygsshd

# Install GPFS
Echo "Beginning GPFS Install"
$licensePath = $ExecutionPath + "\lib\packages\gpfs.ext-5.0.2-Windows-license.msi"
$gpfsPath = $ExecutionPath + "\lib\packages\gpfs.ext-5.0.2.3-Windows.msi"
$gskitPath = $ExecutionPath + "\lib\packages\gpfs.gskit-8.0.50.86.msi"

Start-Process msiexec.exe -Credential $Credential -ArgumentList /i, $licensePath , /passive, AgreeToLicense=yes -WorkingDirectory 'C:\Windows\System32' -Wait
Start-Process msiexec.exe -Credential $Credential -ArgumentList /i, $gpfsPath, /passive  -WorkingDirectory 'C:\Windows\System32' -Wait
Start-Process msiexec.exe -Credential $Credential -ArgumentList /i, $gskitPath, /passive  -WorkingDirectory 'C:\Windows\System32' -Wait

# mmwinserv command

#$mmwinservcmd = "mmwinservctl set --account $domainRootUsername --password " + $RootPassword + " --remote-shell no -N " + $env:COMPUTERNAME
$mmwinservcfg = $ExecutionPath + "\lib\mmwinservcfg.ps1"
#Start-Process $PSHOME\powershell.exe -Credential $Credential -ArgumentList "mmwinservctl","set","--account","$domainRootUsername","--password","$RootPassword","--remote-shell","no","-N","$env:COMPUTERNAME" -WorkingDirectory 'C:\Windows\System32' -Wait
Start-Process $PSHOME\powershell.exe -Credential $Credential -ArgumentList ("-file $mmwinservcfg") -WorkingDirectory 'C:\Windows\System32' -Wait

echo "Install complete!"
pause
