﻿if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$RootUsername = [System.Environment]::GetEnvironmentVariable('ROOTUSERNAME','machine')
$RootPassword = [System.Environment]::GetEnvironmentVariable('ROOTPASSWORD','machine')
C:\cygwin64\bin\sh.exe --login -c 'mkpasswd -l -d >/etc/passwd;mkgroup -l -d >/etc/group;ssh-host-config --yes -u "root" -c "NTSEC" -w "Edison2!";chown root /var/empty;'